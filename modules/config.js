const path = require('path')

module.exports = {
    port: 8001,
    secret: 'Lwcd*dG%$8#SgK?*Gq?&!GsV=!Ax_ES3guUmVDua',
    path: {
        controllers: {
            api: path.resolve('./modules/controllers/api'),
            web: path.resolve('./modules/controllers/web')
        },
        controller: path.resolve('./modules/controllers'),
        transform: path.resolve('./modules/transforms'),
        model: path.resolve('./modules/models')

    }
}