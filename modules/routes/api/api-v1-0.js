/*jshint esversion: 6 */

const express = require('express');
const router = express.Router();

// Middlewares
const apiAuth = require('./middleware/apiAuth');
const apiAdmin = require('./middleware/apiAdmin');

// Controllers
const { api: ControllerAPI } = config.path.controllers;

const controllerAuth = require(`${ControllerAPI}/v1.0/AuthController`);


router.post('/login', controllerAuth.login.bind(controllerAuth));
router.post('/register', controllerAuth.register.bind(controllerAuth));


router.use('/admin', apiAuth, apiAdmin, adminRouter);

module.exports = router;
