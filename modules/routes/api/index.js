const express = require('express');
const router = express.Router();

const apiV1_0 = require('./api-v1-0');

router.use('/v1', apiV1_0);

module.exports = router;
