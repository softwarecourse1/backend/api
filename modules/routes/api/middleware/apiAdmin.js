/*jshint esversion: 6 */

const jwt = require('jsonwebtoken');
const User = require(`${config.path.model}/user`);

module.exports = (req, res, next) => {
	if (req.user.type == 'admin') {
		next();
		return;
	}
	return res.status(403).json({
		data: 'Not Admin',
		successfull: false
	});
};
