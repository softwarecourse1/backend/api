const express = require('express');
const webRouter = express.Router();

webRouter.get('/', (req, res) => {
    res.json('Welcome to Home page');
});
webRouter.get('/about', (req, res) => {
    res.json('Welcome to About page');
});

module.exports = webRouter;